/**
 * View Models used by Spring MVC REST controllers.
 */
package com.iquest.university.web.rest.vm;
